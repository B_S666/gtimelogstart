#!/usr/bin/env bash
GTIMELOG_FILE_PATH=~/.local/share/gtimelog/timelog.txt
CURRENT_START_DATE=$(date '+%Y-%m-%d %H:%M')
PIDOF_RETURN=$(pidof -x "/usr/bin/gtimelog")
if [ "" != "${PIDOF_RETURN}" ]; then
    echo "Already running"
    exit 1
fi

LAST_LINE=1
while [ ${LAST_LINE} -lt 50 ]; do
 LAST_LINE_CONTENT=$(tail -n${LAST_LINE} ${GTIMELOG_FILE_PATH} | head -n1)
 if [ "" != "${LAST_LINE_CONTENT}" ]; then
    break
 fi
 let LAST_LINE=LAST_LINE+1
done

if [ "${CURRENT_START_DATE:0:10}" != "${LAST_LINE_CONTENT:0:10}" ]; then
    echo "${CURRENT_START_DATE}: A new day rises! I bow before you, master!" >> ${GTIMELOG_FILE_PATH}
fi

nohup gtimelog &

while [ true ]; do
	WINDOW_ID=`wmctrl -l | fgrep 'Time Log' | sed 's/\([0-9a-fx]*\).*/\1/'`
	if [ -n "$WINDOW_ID" ]; then
		wmctrl -i -r "$WINDOW_ID" -t 2
		#wmctrl -i -r "$WINDOW_ID" -b "add,maximized_vert,maximized_horz"
		break
	fi
	sleep 1
done
